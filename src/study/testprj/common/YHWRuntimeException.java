package study.testprj.common;

import org.apache.log4j.Logger;

public class YHWRuntimeException extends RuntimeException {

	public YHWRuntimeException(Logger logger, String method,Exception e) {
		logger.error(" Method-->" + method + " " + e.getMessage());
		e.printStackTrace();
	}

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

}
