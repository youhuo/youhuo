package study.testprj.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ibatis.SqlMapClientTemplate;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.ibatis.sqlmap.client.SqlMapClient;
  
public class YHWSqlMapClientDaoSup extends SqlMapClientDaoSupport {
  
	
	protected SqlMapClientTemplate sqlTemplate;

	
	public SqlMapClientTemplate getSqlTemplate() {
		return getSqlMapClientTemplate();
	}  

	public YHWSqlMapClientDaoSup(){
		sqlTemplate = getSqlMapClientTemplate();
	}
	
	@Autowired
	public void setSqlMapClientForAutowire(SqlMapClient sqlMapClient_){
	
	        super.setSqlMapClient(sqlMapClient_);
	}
	
}
