package study.testprj.person.action;

import java.util.List;

import study.testprj.front.domain.Person;
import study.testprj.front.service.PersonService;

import com.opensymphony.xwork2.ActionSupport;

public class ComboBoxAction extends ActionSupport {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	private Person person = new Person();
	
	private List<Person> personList = null;
	
	private PersonService personManager = null;

	@Override
	public String execute() throws Exception {

//		PersonManager pm = (PersonManager) WebApplicationContextUtils
//				.getWebApplicationContext(
//						ServletActionContext.getServletContext()).getBean(
//						"personManager");

		personManager.addPerson(person);

		return SUCCESS;
	}
	
	public String executeForAjax() throws Exception {

		personManager.addPerson(person);

		return SUCCESS;
	}
	
	public String personListInput() throws Exception {

		return "personListInput";
	}
	
	public String personList() throws Exception {

		personList = personManager.getPersonList();
		
		return SUCCESS;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}
	
	public List<Person> getPersonList() {
		return personList;
	}

	public void setPersonManager(PersonService personManager) {
		this.personManager = personManager;
	}

}
