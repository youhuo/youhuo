package study.testprj.person.dao.impl;



//import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import study.testprj.front.dao.PersonDao;
import study.testprj.front.domain.Person;

//public class PersonDaoImpl extends HibernateDaoSupport implements PersonDao {

public class ComboBoxDaoImpl extends SqlMapClientDaoSupport implements PersonDao {
	
	public void addPerson(Person person) {
//		getHibernateTemplate().save(person);
		//person.setId(new Long(104));
		getSqlMapClientTemplate().insert("insertPerson", person);
	}

	@SuppressWarnings("unchecked")
	public List<Person> getPersonList() {
		return getSqlMapClientTemplate().queryForList("selectAllPersons");
	}

	public void delPerson(int id) {
		// TODO Auto-generated method stub
		
	}
	
	

}
