package study.testprj.person.manager;

import java.util.List;

import study.testprj.front.domain.Person;

public interface ComboBoxManager {
	
	void addPerson(Person person);
	
	List<Person> getPersonList();
	
//	void updatePerson(Session session, Person person);
//	
//	Person getPerson(Session session, Person person);
//	
//	void deletePerson(Session session, Person person);

}
