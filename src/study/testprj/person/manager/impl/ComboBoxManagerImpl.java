package study.testprj.person.manager.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import study.testprj.front.dao.PersonDao;
import study.testprj.front.domain.Person;
import study.testprj.front.service.PersonService;

@Transactional
public class ComboBoxManagerImpl implements PersonService {

	private PersonDao personDao = null;
	
	
	public void setPersonDao(PersonDao personDao) {
		this.personDao = personDao;
	}

	@Transactional(readOnly=false)
	public void addPerson(Person person) {
		personDao.addPerson(person);
	}

	public List<Person> getPersonList() {
		return personDao.getPersonList();
	}

}
