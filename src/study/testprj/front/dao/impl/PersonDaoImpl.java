package study.testprj.front.dao.impl;

//import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import org.springframework.stereotype.Repository;

import study.testprj.common.YHWSqlMapClientDaoSup;
import study.testprj.front.dao.PersonDao;
import study.testprj.front.domain.Person;

//public class PersonDaoImpl extends HibernateDaoSupport implements PersonDao {
@Repository("personDaoImpl")
public class PersonDaoImpl extends YHWSqlMapClientDaoSup implements PersonDao {
	
	public void addPerson(Person person) {  
//		getHibernateTemplate().save(person);
		//person.setId(new Long(104));
		sqlTemplate.insert("insertPerson", person);
	}
  
	@SuppressWarnings("unchecked")
	public List<Person> getPersonList() {
		return sqlTemplate.queryForList("selectAllPersons");
	}

	public void delPerson(int id) {
		
		sqlTemplate.delete("deletePersonById", id);
		
	}
	

}
