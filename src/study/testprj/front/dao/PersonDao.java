package study.testprj.front.dao;

import java.util.List;

import study.testprj.front.domain.Person;

public interface PersonDao {

	void delPerson(int id);
	
	void addPerson(Person person);
	
	List<Person> getPersonList();
}
