package study.testprj.front.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import study.testprj.front.dao.PersonDao;
import study.testprj.front.domain.Person;
import study.testprj.front.service.PersonService;

@Service("personServiceImpl")
public class PersonServiceImpl implements PersonService {

	@Autowired
	@Qualifier("personDaoImpl")  // field 注入，Qualifier id 唯一确定值
	private PersonDao personDao;
	

	@Transactional
	public void addPerson(Person person) {
//		personDao.delPerson(2);
		
		personDao.addPerson(person);
		int y = 7/0;
		
	}

	public List<Person> getPersonList() {
		return personDao.getPersonList();
	}

}
