package study.testprj.front.action;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import study.testprj.common.YHWBaseAction;
import study.testprj.common.YHWRuntimeException;
import study.testprj.front.domain.Person;
import study.testprj.front.service.PersonService;

@Controller
@Scope("prototype")
public class PersonAction extends YHWBaseAction {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	static Logger logger = Logger.getLogger(PersonAction.class);

	private Person person = new Person();
	
	private List<Person> personList = null;
	
	@Autowired
	private PersonService personService;

	@Override
	public String execute() throws Exception {

//		PersonManager pm = (PersonManager) WebApplicationContextUtils
//				.getWebApplicationContext(
//						ServletActionContext.getServletContext()).getBean(
//						"personManager");
		
		personService.addPerson(person);

		return SUCCESS;
	}
	
	
	public String addPerson() throws Exception {
		
		try {
			person.setAge(36);
			person.setFirstName("guo");
			person.setLastName("����");
			personService.addPerson(person);

			
		} catch (Exception e) {
			if(e instanceof RuntimeException){
				new YHWRuntimeException(logger,"addPerson",e);
			}
			else{
				throw e;
			}
		}
		return SUCCESS;
	}
	
	public String executeForAjax() throws Exception {

		personService.addPerson(person);

		return SUCCESS;
	}
	
	public String personListInput() throws Exception {

		return "personListInput";
	}
	
	public String personList() throws Exception {

		personList = personService.getPersonList();
		
		return SUCCESS;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}
	
	public List<Person> getPersonList() {
		return personList;
	}


}
