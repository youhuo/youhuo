<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>personInput</title>
<s:head />
<script type="text/javascript" src="<%=request.getContextPath()%>/ext/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/ext/ext-all.js"></script>
<script type="text/javascript">
function Params() {
	
}
function saveClick()
{
	var params = new Object();
	
	var url = '<%=request.getContextPath()%>/personAdd.action';
	params.person = (new Ext.form.BasicForm("personInput")).getValues(); 
	//params.person = document.getElementById("personInput");
	var jsonStr = Ext.encode(params);
	
	Ext.Ajax.request(
	{	url: url,
		method:'post',
		params:jsonStr,
		headers:{"Content-Type":"application/json; charset=utf-8"},
		success: processResponse
	});
	
}

function processResponse(request)
{
	Ext.MessageBox.alert("Hi", "Success");
}

</script>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/ext/resources/css/ext-all.css"></link>
</head>
<body>
<s:form>
 	  <s:textfield key="firstName" />
 	  <s:textfield  key="lastName" />
	  <s:textfield  key="age" />
	  <input type="button" name="submit" value="submit" onclick="saveClick();"/>

</s:form>	
	
 
</body>
</html>