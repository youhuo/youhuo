<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>personInput</title>
<s:head />
<script type="text/javascript" src="<%=request.getContextPath()%>/ext/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/ext/ext-all.js"></script>

<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/ext/resources/css/ext-all.css"></link>
</head>
<body>
	<div id="myGrid">

	</div>
<script type="text/javascript">

var person_sm = new Ext.grid.CheckboxSelectionModel();
var person_Record = Ext.data.Record.create([ 
                                             {name: 'id', type: 'string'},
                                             {name: 'firstName', type: 'string'},
                                             {name: 'lastName', type: 'string'},
                                             {name: 'age', type: 'string'}
                                         	]);
	
var person_reader = new Ext.data.JsonReader({
    root: 'personList'},
    person_Record );

var person_ds = new Ext.data.Store({                                 
    proxy: new Ext.data.HttpProxy({url: "study/personList.action"}),                                                                         
    remoteSort :false,
    reader: person_reader                                             
});  

person_ds.load();

var person_grid = new Ext.grid.GridPanel({                
	store: person_ds,    
	columns: [                                    
	    		person_sm,            
			    { header: "ID", width: 100, sortable: true, hidden: false, dataIndex: 'id', align: 'center' , renderer:  null},
			    { header: "FirstName", width: 100, sortable: true, hidden: false, dataIndex: 'firstName', align: 'center' , renderer:  null},
			    { header: "LastName", width: 200, sortable: true, hidden: false, dataIndex: 'lastName', align: 'center' , renderer:  null},
			    { header: "Age", width: 100, sortable: true, hidden: false, dataIndex: 'age', align: 'center' , renderer:  null}
			],
	viewConfig: {forceFit: true},                                                    
	autoScroll: true,                      
	stripeRows: true,
	border:	false,
	height: 350,
    width: 600,
	sm:	person_sm	             
}); 

person_grid.render('myGrid');


</script>
</body>

</html>