<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>personInput</title>
<s:head />
<script type="text/javascript" src="<%=request.getContextPath()%>/ext/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/ext/ext-all.js"></script>
<script type="text/javascript">

Ext.onReady(function() {

    Ext.QuickTips.init();

    // turn on validation errors beside the field globally
    Ext.form.Field.prototype.msgTarget = 'side';

    //var bd = Ext.getBody();

    /*
     * ================  Simple form  =======================
     */
    //bd.createChild({tag: 'h2', html: 'Form 1 - Very Simple'});


    var simple = new Ext.form.FormPanel({
        id: 'simpleFormPanel', 
        labelWidth: 75, // label settings here cascade unless overridden
        url:'anyThing',
        frame:true,
        title: 'Simple Form',
        bodyStyle:'padding:5px 5px 0',
        width: 350,
        defaults: {width: 230},
        defaultType: 'textfield',
		applyTo:'form',
        items: 
           [{
                fieldLabel: 'First Name',
                name: 'firstName',
                allowBlank:false
            },{
                fieldLabel: 'Last Name',
                name: 'lastName'
            },{
                fieldLabel: 'Age',
                name: 'age'
            }
        ],

        buttons: [{
            	text: 'Save',
            	handler : saveClick
        }]
    });

    //simple.render(document.body);
});

function saveClick()
{
	var url = '<%=request.getContextPath()%>/personAdd.action';
	var params = new Object();
	
	params.person = (Ext.getCmp('simpleFormPanel')).getForm().getValues();
	
	var jsonStr = Ext.encode(params);

	Ext.MessageBox.alert("xxx", jsonStr);

	Ext.Ajax.request(
			{	url: url,
				method:'post',
				params:jsonStr,
				headers:{"Content-Type":"application/json; charset=utf-8"},
				success: processResponse
			});
	
}

function processResponse(request)
{
	Ext.MessageBox.alert("Hi", "Success");
}

</script>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/ext/resources/css/ext-all.css"></link>
</head>
<body>
	<div id="form">

	</div>
</body>
</html>