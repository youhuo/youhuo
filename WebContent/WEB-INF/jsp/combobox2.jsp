<%@ page language="java" import="java.util.*" pageEncoding="GB18030"%>



<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>My JSP 'index.jsp' starting page</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<link rel="stylesheet" type="text/css" href="ext-3.0.0/resources/css/ext-all.css">
	<script type="text/javascript" src="ext-3.0.0/adapter/ext/ext-base.js"></script>
	<script type="text/javascript" src="ext-3.0.0/ext-all.js"></script>
	<script type="text/javascript">
	Ext.onReady(function(){
		var provinceStore = new Ext.data.Store({
   		autoLoad : false,  //设置为不自动读取数据
   		proxy : new Ext.data.HttpProxy({
    		url : 'searchCountry.action'
   		}),
   		reader : new Ext.data.JsonReader({
     	root : "provinceList", //回传的数据集合，名称与action中的属性名一致
     	id : "provinceStore_id"
    	},[
     	{name : 'provinceName',mapping : 'provinceName'},
     	{name : 'provinceId',mapping : 'provinceId'}
   		]
   		)
  		});
  /**

   *放置city信息的Store

   */
  var cityStore= new Ext.data.Store({
   autoLoad : false, 
   proxy : new Ext.data.HttpProxy({
    url : 'searchCityList.html',
    method : 'POST'
   }),
   reader : new Ext.data.ArrayReader({
    root : 'cityList',
    id : 'cityStore_id'
   }, [
    {name : 'cityName',mapping : 'cityName'},

    {name : 'cityId',mapping : 'cityId'}
    ]
   )
  });

	items : [

     {
    fieldLabel : '省份,
    xtype : 'combo',
    store : provinceStore,
    id : 'provinceCombo',
    triggerAction : 'all',//默认为"query"如果不是all的话，选择一次以后其他的值就不见了
    readOnly :true,//只读

    mode : 'remote',//远程模式

    name : 'provinceCombo',
    valueField : "provinceId",//实际在请求中传递的值
    displayField : "provinceName",//显示在下拉框里的值
    handler : function () {
     proviceStore.load();//读取省份的信息
    },
    listeners : {
     select : function(combo, record, index) {
      //每次重新选中的时候把cityCombo的值设置为空
      //当然也可以通过这个方法为cityCombo设置一个初始值，依据具体应用而定

      //this.getValue（这个是Id值）或者record.data.provinceName把这个放到setValue里就可以做到了。

      Ext.getCmp('cityCombo').setValue('');

      cityStore.load({
       params : {
        provinceId : this.getValue()

        //cityStore向'searchCityList.html'请求是附带参数provinceId，可以根据这个值查询出某个省相应的城市信息
       }
      });
     }
    }
    }, {
     fieldLabel : '城市',
     xtype : 'combo',
     store : cityStore,
     readOnly : true,
     id : 'cityCombo',
     mode : 'local',//本地模式
     name : 'cityCombo',
     valueField : "cityId",

     triggerAction : 'all', 
     displayField : "cityName"

    }

 ]

	
	
	
　　　　　　form.render(document.body);　
	});
	</script>
  </head>

  <body>
    <div id="combo"></div>


  </body>
</html>
